codeunit 50110 "nH Cases Card Ext Tests"
{
    Subtype = Test;

    [Test]
    [HandlerFunctions('TestSendClosedEmailMessageHandler_NotClosed')]
    procedure ClosedEmail_CaseNotClosed()
    var
        nHCaseCard: TestPage "Case Card nH";
        SMTP: Codeunit "Email Test Mail";
    begin
        InsertCaseRecord();
        TestCaseRecordValues();
        nHCaseCard.OpenEdit();
        nHCaseCard.First();
        //test the email button displays correct message when pressed while status <> closed
        nHCaseCard."Send Closed Email".Invoke();
        Assert.AreEqual(true, caseClosed, 'Correct message displayed when case is not closed');
        nHCaseCard.Close();
    end;

    [Test]
    [Scope('OnPrem')]
    [HandlerFunctions('TestSendClosedEmailMessageHandler_Closed,CloseCaseConfirmHandler')]
    procedure ClosedEmail_CaseClosed()
    var
        nHCaseCard: TestPage "Case Card nH";
        LibraryWorkflow: Codeunit "Library - Workflow";
        Contact: Record Contact;
    begin
        InsertCaseRecord();
        TestCaseRecordValues();
        nHCaseCard.OpenEdit();
        nHCaseCard.First();
        if Contact.get(nHCasesPersonContactNo) then begin
            Contact.Validate("E-Mail", 'test@test.com');
            Contact.Modify(true);
        end;
        LibraryWorkflow.SetUpEmailAccount();
        nHCaseCard.Status.Value := 'CLOSED';
        nHCaseCard."Send Closed Email".Invoke();
        Assert.AreEqual(true, caseClosed, 'Correct message displayed when case closed');
    end;

    [Test]
    [HandlerFunctions('TestSendClosedEmailMessageHandler_NoContactEmail,CloseCaseConfirmHandler')]
    procedure CloseEmail_NoContactEmail()
    var
        nHCaseCard: TestPage "Case Card nH";
        contactEmail: Text;
        contact: Record Contact;
    begin
        InsertCaseRecord();
        TestCaseRecordValues();
        nHCaseCard.OpenEdit();
        nHCaseCard.First();

        if contact.get(nHCaseCard."Person Contact No.".Value) then begin
            contactEmail := contact."E-Mail";
            contact."E-Mail" := '';
            contact.Modify(true);
            nHCaseCard.Status.Value := 'CLOSED';
            nHCaseCard."Send Closed Email".Invoke();
            Assert.AreEqual(true, caseClosed, 'Correct message displayed when no contact email');
            contact."E-Mail" := contactEmail;
            contact.Modify(true);
        end else begin
            Error('Contact not found');
        end;
    end;

    [ConfirmHandler]
    procedure CloseCaseConfirmHandler(Question: Text[1024]; var Reply: Boolean)
    begin
        if Question = closeConfirmQuestion then Reply := true;
    end;

    [MessageHandler]
    procedure TestSendClosedEmailMessageHandler_NotClosed(Message: Text[1024])
    begin
        caseClosed := Message = caseNotClosedMessage;
    end;

    [MessageHandler]
    procedure TestSendClosedEmailMessageHandler_Closed(Message: Text[1024])
    begin
        caseClosed := Message = caseClosedMessage;
    end;

    [MessageHandler]
    procedure TestSendClosedEmailMessageHandler_NoContactEmail(Message: Text[1024])
    begin
        caseClosed := Message = caseNoContactEmailMessage;
    end;

    local procedure InsertCaseRecord();
    var
        nHCases: Record "Case Header nH";
        LibraryMarketing: Codeunit "Library - Marketing";
        Contact: Record Contact;
    begin
        InsertRelatedData();
        nHCases.DeleteAll(true);
        nHCases.Init();
        nHCases.Validate(Description, nHCasesDescription);
        nHCases.Insert(true);
        nHCases.Reset();
        LibraryMarketing.CreatePersonContactWithCompanyNo(Contact);
        nHCasesCompanyContactNo := Contact."Company No.";
        nHCasesPersonContactNo := Contact."No.";
        if nHCases.FindFirst() then begin
            nHCases.Validate("Case Date", Today());
            nHCases.Validate("Type", nHCasesCaseType);
            nHCases.Validate(SLA, nHCasesSLA);
            nHCases.Validate(Priority, nHCasesPriority);
            nHCases.Validate("Company Contact No.", Contact."Company No.");
            nHCases.Validate(Status, nHCasesStatus);
            nHCases.Modify(true);
        end;
    end;

    local procedure TestCaseRecordValues()
    var
        nHCaseCard: TestPage "Case Card nH";
        nHCases: Record "Case Header nH";
    begin
        nHCaseCard.OpenEdit();
        nHCaseCard.First();
        if nHCases.Get(nHCaseCard."No.".Value) then;
        Assert.AreEqual(nHCasesDescription, nHCaseCard.Description.Value, 'Description is correct');
        //Assert.AreEqual(nHCaseCard."Case Date".Value, Today(), 'Date is today');
        Assert.AreEqual(nHCasesCaseType, nHCaseCard."Type".Value, 'Type is correct');
        Assert.AreEqual(nHCasesSLA, nHCaseCard.SLA.Value, 'SLA is correct');
        Assert.AreEqual(nHCasesPriority, nHCaseCard.Priority.Value, 'Priority is correct');
        Assert.AreEqual(nHCasesCompanyContactNo, nHCaseCard."Company Contact No.".Value, 'Contact No is correct');
        Assert.AreEqual(nHCasesPersonContactNo, nHCaseCard."Person Contact No.".Value, 'Person Contact No. autofilled correct');
        Assert.AreEqual(nHCasesStatus, nHCaseCard.Status.Value, 'Status is correct');
    end;

    local procedure InsertRelatedData()
    var
        nHCaseTypes: Record "Case Type nH";
        nHCaseSLA: Record "Case SLA nH";
        nHCasePriority: Record "Case Priority nH";
        nHCaseStatus: Record "Case Status nH";
    begin
        nHCaseTypes.Reset();
        if not nHCaseTypes.get(nHCasesCaseType) then begin
            nHCaseTypes.Init();
            nHCaseTypes.Validate(Code, nHCasesCaseType);
            nHCaseTypes.Insert(true);
        end;
        nHCaseSLA.Reset();
        if not nHCaseSLA.get(nHCasesSLA) then begin
            nHCaseSLA.Init();
            nHCaseSLA.Validate(Code, nHCasesSLA);
            nHCaseSLA.Insert(true);
        end;
        nHCasePriority.Reset();
        if not nHCasePriority.get(nHCasesPriority) then begin
            nHCasePriority.Init();
            nHCasePriority.Validate(Code, nHCasesPriority);
            nHCasePriority.Insert(true);
        end;
        nHCaseStatus.Reset();
        if not nHCaseStatus.get(nHCasesCaseType, nHCasesStatus) then begin
            nHCaseStatus.Init();
            nHCaseStatus.Validate(Type, nHCasesCaseType);
            nHCaseStatus.Validate(Code, nHCasesStatus);
            nHCaseStatus.Validate(Initial, true);
            nHCaseStatus.Insert(true);
        end;
        nHCaseStatus.Reset();
        if not nHCaseStatus.get(nHCasesCaseType, 'CLOSED') then begin
            nHCaseStatus.Init();
            nHCaseStatus.Validate(Type, nHCasesCaseType);
            nHCaseStatus.Validate(Code, 'CLOSED');
            nHCaseStatus.Validate("Closes Case", true);
            nHCaseStatus.Insert(true);
        end;
    end;

    var
        Assert: Codeunit Assert;
        nHCasesDescription: Label 'Sample Test Case';
        nHCasesCaseType: Label 'GENERAL QUERY';
        nHCasesSLA: Label 'QUERY';
        nHCasesPriority: Label 'LOW';
        nHCasesCompanyContactNo: Code[20];
        nHCasesPersonContactNo: Code[20];
        nHCasesStatus: Label 'LOGGED';
        caseClosed: Boolean;
        caseClosedMessage: Label 'Message sent!';
        caseNotClosedMessage: Label 'You need to close the case first';
        caseNoContactEmailMessage: Label 'Contact Email is blank';
        closeConfirmQuestion: Label 'Confirm Do you want to close case "%1"?';
        senderName: Label 'Richard';
        senderEmail: Label 'richard.sefton@thenavpeople.co.uk';
}