pageextension 50100 "nH Cases Card Ext" extends "Case Card nH"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addafter(ReleaseAction)
        {
            action("Send Closed Email")
            {
                ApplicationArea = All;
                Caption = 'Send Closed Email';
                Image = Email;
                Visible = true;
                ToolTip = 'Send an email once the Case is marked as closed';
                trigger OnAction()
                var
                    closeFirstMsg: Label 'You need to close the case first';
                begin
                    if Rec.Status <> 'CLOSED' then begin
                        Message(closeFirstMsg);
                        exit;
                    end else begin
                        Message(SendEmail());
                    end;
                end;
            }
        }
    }

    local procedure SendEmail(): Text
    var
        contact: Record Contact;
        emailMessage: Codeunit "Email Message";
        email: Codeunit Email;
        recipients: List of [Text];
        subject: Text;
        body: Text;
        emailMsgTemplate: Label 'Hi %1, <br/>. The case no %2 has been closed.<br/><br/>Thanks<br/>%3';
    begin
        if contact.get(Rec."Person Contact No.") then begin
            if contact."E-Mail" = '' then exit('Contact Email is blank');
            recipients.Add(contact."E-Mail");
            subject := Rec."No.";
            body := StrSubstNo(emailMsgTemplate, contact.Name, Rec."No.", Rec."Assigned To Contact Name");
            emailMessage.Create(recipients, subject, body, true);
            email.Send(emailMessage, Enum::"Email Scenario"::Default);
            exit('Message sent!');
        end;
    end;
}